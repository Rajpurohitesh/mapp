# Author:Hitesh
FROM openjdk:11-jdk
COPY target/mobilestore-0.0.1-SNAPSHOT.jar mobilestore-0.0.1-SNAPSHOT.jar
CMD ["java","-jar","mobilestore-0.0.1-SNAPSHOT.jar"]
EXPOSE 8099
